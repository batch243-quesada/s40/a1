const express = require('express');
const auth = require('../auth');

const router = express.Router();

const courseControllers = require('../controllers/courseControllers');


router.post("/", auth.verify, courseControllers.addCourse);

router.get("/all-active-courses", courseControllers.getAllActive);

router.get("/:courseId", courseControllers.getCourse);

router.put("/update/:courseId", auth.verify, courseControllers.updateCourse)

router.patch("/update/:courseId/archive", auth.verify, courseControllers.archiveCourse)


module.exports = router;