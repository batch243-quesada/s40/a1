const Course = require('../models/Courses');
const User = require('../models/Users');
const	auth = require('../auth');

// 1. Create a new Course object using mongoose model and information from the request
// 2. Save the new course to the database
const addCourse = (request, response) => {
	let newCourse = new Course({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots
	})

	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin) {
		return newCourse
		.save()
		.then(result => {
			console.log(result)
			response.send(true)
		}).catch(error => {
			console.log(error);
			response.send(false);
		})
	} else {
		return response.send('Access denied.');
	}
}

const getAllActive = (request, response) => {
	return Course.find({isActive : true})
	.then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// retrieve a specific course
const getCourse = (request, response) => {
	const courseId = request.params.courseId;

	return Course.findById(courseId).then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// update a course
const updateCourse = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots
	}

	const courseId = request.params.courseId;
	if(userData.isAdmin) {
		return Course.findByIdAndUpdate(courseId, updatedCourse, {new:true})
		.then(result => {
			response.send(result);
		}).catch(err => {
			response.send(err);
		})
	} else {
		return response.send("Access denied.");
	}
}

const archiveCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	const courseId = request.params.courseId;

	let archivedCourse = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin) {
		return Course.findByIdAndUpdate(courseId, archivedCourse, {new:true})
		.then(result => {
			response.send(true);
		}).catch(err =>{
			response.send	(err);
		})
	} else {
		response.send('Access denied.')
	}
}

// const archiveCourse = (request, response) => {
// 	const userData = auth.decode(request.headers.authorization);
// 	console.log(userData);

// 	let patchedCourse = {
// 		isActive: request.body.isActive
// 	}

// 	const courseId = request.params.courseId;

// 	if(userData.isAdmin) {
// 		if(Course.slots === 0) {
// 			return Course.findByIdAndUpdate(courseId, patchedCourse, {new: true})
// 			.then(result => {
// 				console.log(result);
// 				response.send(true);
// 			}).catch(error => {
// 				console.log(error);
// 				response.send(false);
// 			})
// 		} else {
// 			return response.send(`There are ${Course.slots} left for this course.`);
// 		}
// 	} else {
// 		return response.send("Access denied.");
// 	}
// }

module.exports = {
	addCourse,
	getAllActive,
	getCourse,
	updateCourse,
	archiveCourse
}